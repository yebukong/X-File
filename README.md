# X-File[文件操作小工具]

#### 介绍(暂)

- 对大文本文件进行切割复制
- 支持自定义切割大小
- 支持进度条


#### 软件架构
1. 运行环境 Microsoft .NET Framework4.0
2. 基于[DMSkin for WinForm](http://www.dmskin.com/) 开发


#### 使用

1. 下载源码进行编译(项目于vs2013开发)
2. 下载[附件](https://gitee.com/yebukong/X-File/attach_files)直接运行

#### 截图
![主页面](https://images.gitee.com/uploads/images/2018/1221/161655_cf3508bf_884684.png "主页面")
