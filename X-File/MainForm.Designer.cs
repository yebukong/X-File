﻿namespace X_File
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.dmTabControl = new DMSkin.Controls.DMTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.metroButtonStart = new DMSkin.Metro.Controls.MetroButton();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dmTextBoxLog = new DMSkin.Controls.DMTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelSplitSize = new System.Windows.Forms.Panel();
            this.metroRadioButtonSizeTwo = new DMSkin.Metro.Controls.MetroRadioButton();
            this.metroRadioButtonSizeOther = new DMSkin.Metro.Controls.MetroRadioButton();
            this.metroLabel6 = new DMSkin.Metro.Controls.MetroLabel();
            this.metroRadioButtonSizeOne = new DMSkin.Metro.Controls.MetroRadioButton();
            this.dmTextBoxOther = new DMSkin.Controls.DMTextBox();
            this.panelSplitType = new System.Windows.Forms.Panel();
            this.metroRadioButton3 = new DMSkin.Metro.Controls.MetroRadioButton();
            this.metroRadioButton2 = new DMSkin.Metro.Controls.MetroRadioButton();
            this.metroRadioButton1 = new DMSkin.Metro.Controls.MetroRadioButton();
            this.metroLabel5 = new DMSkin.Metro.Controls.MetroLabel();
            this.metroLabel4 = new DMSkin.Metro.Controls.MetroLabel();
            this.metroButtonTargetPath = new DMSkin.Metro.Controls.MetroButton();
            this.metroButtonSoucres = new DMSkin.Metro.Controls.MetroButton();
            this.dmTextBoxTargetPath = new DMSkin.Controls.DMTextBox();
            this.metroLabel3 = new DMSkin.Metro.Controls.MetroLabel();
            this.metroLabel2 = new DMSkin.Metro.Controls.MetroLabel();
            this.dmTextBoxSourceFile = new DMSkin.Controls.DMTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.metroLabel1 = new DMSkin.Metro.Controls.MetroLabel();
            this.dmIcon1 = new DMSkin.Controls.DM.DMIcon();
            this.dmTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelSplitSize.SuspendLayout();
            this.panelSplitType.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dmTabControl
            // 
            this.dmTabControl.BackColor = System.Drawing.Color.Transparent;
            this.dmTabControl.Controls.Add(this.tabPage1);
            this.dmTabControl.Controls.Add(this.tabPage2);
            this.dmTabControl.DMCheckBackColor = System.Drawing.Color.DarkGray;
            this.dmTabControl.DMCheckColor = System.Drawing.Color.Gainsboro;
            this.dmTabControl.DMDrawBack = true;
            this.dmTabControl.DMHeight = 3;
            this.dmTabControl.DMHoverBackColor = System.Drawing.Color.LightGray;
            this.dmTabControl.DMHoverColor = System.Drawing.Color.Gainsboro;
            this.dmTabControl.DMNormalBackColor = System.Drawing.Color.Gainsboro;
            this.dmTabControl.DMNormalColor = System.Drawing.Color.Gainsboro;
            this.dmTabControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dmTabControl.ItemSize = new System.Drawing.Size(80, 32);
            this.dmTabControl.Location = new System.Drawing.Point(4, 52);
            this.dmTabControl.Multiline = true;
            this.dmTabControl.Name = "dmTabControl";
            this.dmTabControl.SelectedIndex = 0;
            this.dmTabControl.Size = new System.Drawing.Size(692, 464);
            this.dmTabControl.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.dmTabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage1.Location = new System.Drawing.Point(4, 36);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(684, 424);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "大文本切割";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.metroButtonStart);
            this.groupBox3.Controls.Add(this.progressBar);
            this.groupBox3.Location = new System.Drawing.Point(23, 189);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(639, 58);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "进度";
            // 
            // metroButtonStart
            // 
            this.metroButtonStart.DM_UseSelectable = true;
            this.metroButtonStart.Location = new System.Drawing.Point(19, 19);
            this.metroButtonStart.Name = "metroButtonStart";
            this.metroButtonStart.Size = new System.Drawing.Size(52, 30);
            this.metroButtonStart.TabIndex = 18;
            this.metroButtonStart.Text = "开始";
            this.metroButtonStart.Click += new System.EventHandler(this.metroButtonStart_Click);
            // 
            // progressBar
            // 
            this.progressBar.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.progressBar.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.progressBar.Location = new System.Drawing.Point(77, 19);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(544, 30);
            this.progressBar.TabIndex = 0;
            this.progressBar.Tag = "";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.dmTextBoxLog);
            this.groupBox2.Location = new System.Drawing.Point(22, 256);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(639, 150);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "工作区";
            // 
            // dmTextBoxLog
            // 
            this.dmTextBoxLog.BackColor = System.Drawing.Color.LightGray;
            this.dmTextBoxLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dmTextBoxLog.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.dmTextBoxLog.Location = new System.Drawing.Point(6, 22);
            this.dmTextBoxLog.Multiline = true;
            this.dmTextBoxLog.Name = "dmTextBoxLog";
            this.dmTextBoxLog.ReadOnly = true;
            this.dmTextBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dmTextBoxLog.Size = new System.Drawing.Size(627, 118);
            this.dmTextBoxLog.TabIndex = 1;
            this.dmTextBoxLog.Text = "_(:3」∠)_ ";
            this.dmTextBoxLog.WaterText = "";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.panelSplitSize);
            this.groupBox1.Controls.Add(this.panelSplitType);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroButtonTargetPath);
            this.groupBox1.Controls.Add(this.metroButtonSoucres);
            this.groupBox1.Controls.Add(this.dmTextBoxTargetPath);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.dmTextBoxSourceFile);
            this.groupBox1.Location = new System.Drawing.Point(23, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(639, 169);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "切割配置";
            // 
            // panelSplitSize
            // 
            this.panelSplitSize.Controls.Add(this.metroRadioButtonSizeTwo);
            this.panelSplitSize.Controls.Add(this.metroRadioButtonSizeOther);
            this.panelSplitSize.Controls.Add(this.metroLabel6);
            this.panelSplitSize.Controls.Add(this.metroRadioButtonSizeOne);
            this.panelSplitSize.Controls.Add(this.dmTextBoxOther);
            this.panelSplitSize.Location = new System.Drawing.Point(90, 54);
            this.panelSplitSize.Name = "panelSplitSize";
            this.panelSplitSize.Size = new System.Drawing.Size(542, 24);
            this.panelSplitSize.TabIndex = 17;
            // 
            // metroRadioButtonSizeTwo
            // 
            this.metroRadioButtonSizeTwo.AutoSize = true;
            this.metroRadioButtonSizeTwo.DM_UseSelectable = true;
            this.metroRadioButtonSizeTwo.Location = new System.Drawing.Point(85, 4);
            this.metroRadioButtonSizeTwo.Name = "metroRadioButtonSizeTwo";
            this.metroRadioButtonSizeTwo.Size = new System.Drawing.Size(57, 17);
            this.metroRadioButtonSizeTwo.TabIndex = 10;
            this.metroRadioButtonSizeTwo.Tag = "524288000";
            this.metroRadioButtonSizeTwo.Text = "500M";
            // 
            // metroRadioButtonSizeOther
            // 
            this.metroRadioButtonSizeOther.AutoSize = true;
            this.metroRadioButtonSizeOther.DM_UseSelectable = true;
            this.metroRadioButtonSizeOther.Location = new System.Drawing.Point(186, 4);
            this.metroRadioButtonSizeOther.Name = "metroRadioButtonSizeOther";
            this.metroRadioButtonSizeOther.Size = new System.Drawing.Size(48, 17);
            this.metroRadioButtonSizeOther.TabIndex = 9;
            this.metroRadioButtonSizeOther.Tag = "other";
            this.metroRadioButtonSizeOther.Text = "其他";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(332, 3);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(27, 20);
            this.metroLabel6.TabIndex = 15;
            this.metroLabel6.Text = "KB";
            // 
            // metroRadioButtonSizeOne
            // 
            this.metroRadioButtonSizeOne.AutoSize = true;
            this.metroRadioButtonSizeOne.Checked = true;
            this.metroRadioButtonSizeOne.DM_UseSelectable = true;
            this.metroRadioButtonSizeOne.Location = new System.Drawing.Point(4, 4);
            this.metroRadioButtonSizeOne.Name = "metroRadioButtonSizeOne";
            this.metroRadioButtonSizeOne.Size = new System.Drawing.Size(45, 17);
            this.metroRadioButtonSizeOne.TabIndex = 8;
            this.metroRadioButtonSizeOne.TabStop = true;
            this.metroRadioButtonSizeOne.Tag = "104857600";
            this.metroRadioButtonSizeOne.Text = "100";
            // 
            // dmTextBoxOther
            // 
            this.dmTextBoxOther.BackColor = System.Drawing.Color.Snow;
            this.dmTextBoxOther.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dmTextBoxOther.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.dmTextBoxOther.Location = new System.Drawing.Point(252, 0);
            this.dmTextBoxOther.Name = "dmTextBoxOther";
            this.dmTextBoxOther.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dmTextBoxOther.Size = new System.Drawing.Size(74, 23);
            this.dmTextBoxOther.TabIndex = 13;
            this.dmTextBoxOther.Text = "1024";
            this.dmTextBoxOther.WaterText = "";
            this.dmTextBoxOther.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dmTextBoxOther_KeyPress);
            // 
            // panelSplitType
            // 
            this.panelSplitType.Controls.Add(this.metroRadioButton3);
            this.panelSplitType.Controls.Add(this.metroRadioButton2);
            this.panelSplitType.Controls.Add(this.metroRadioButton1);
            this.panelSplitType.Location = new System.Drawing.Point(90, 23);
            this.panelSplitType.Name = "panelSplitType";
            this.panelSplitType.Size = new System.Drawing.Size(542, 22);
            this.panelSplitType.TabIndex = 16;
            // 
            // metroRadioButton3
            // 
            this.metroRadioButton3.AutoSize = true;
            this.metroRadioButton3.DM_UseSelectable = true;
            this.metroRadioButton3.Location = new System.Drawing.Point(85, 3);
            this.metroRadioButton3.Name = "metroRadioButton3";
            this.metroRadioButton3.Size = new System.Drawing.Size(72, 17);
            this.metroRadioButton3.TabIndex = 10;
            this.metroRadioButton3.Tag = "2";
            this.metroRadioButton3.Text = "等量截取";
            // 
            // metroRadioButton2
            // 
            this.metroRadioButton2.AutoSize = true;
            this.metroRadioButton2.Checked = true;
            this.metroRadioButton2.DM_UseSelectable = true;
            this.metroRadioButton2.Location = new System.Drawing.Point(186, 2);
            this.metroRadioButton2.Name = "metroRadioButton2";
            this.metroRadioButton2.Size = new System.Drawing.Size(60, 17);
            this.metroRadioButton2.TabIndex = 9;
            this.metroRadioButton2.TabStop = true;
            this.metroRadioButton2.Tag = "3";
            this.metroRadioButton2.Text = "尾截取";
            // 
            // metroRadioButton1
            // 
            this.metroRadioButton1.AutoSize = true;
            this.metroRadioButton1.DM_UseSelectable = true;
            this.metroRadioButton1.Location = new System.Drawing.Point(4, 3);
            this.metroRadioButton1.Name = "metroRadioButton1";
            this.metroRadioButton1.Size = new System.Drawing.Size(60, 17);
            this.metroRadioButton1.TabIndex = 8;
            this.metroRadioButton1.Tag = "1";
            this.metroRadioButton1.Text = "头截取";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(6, 54);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(79, 20);
            this.metroLabel5.TabIndex = 7;
            this.metroLabel5.Text = "截取大小：";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(6, 21);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(79, 20);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "截取方式：";
            // 
            // metroButtonTargetPath
            // 
            this.metroButtonTargetPath.DM_UseSelectable = true;
            this.metroButtonTargetPath.Location = new System.Drawing.Point(583, 124);
            this.metroButtonTargetPath.Name = "metroButtonTargetPath";
            this.metroButtonTargetPath.Size = new System.Drawing.Size(44, 23);
            this.metroButtonTargetPath.TabIndex = 5;
            this.metroButtonTargetPath.Text = "选择";
            this.metroButtonTargetPath.Click += new System.EventHandler(this.metroButtonTargetPath_Click);
            // 
            // metroButtonSoucres
            // 
            this.metroButtonSoucres.DM_UseSelectable = true;
            this.metroButtonSoucres.Location = new System.Drawing.Point(584, 84);
            this.metroButtonSoucres.Name = "metroButtonSoucres";
            this.metroButtonSoucres.Size = new System.Drawing.Size(43, 23);
            this.metroButtonSoucres.TabIndex = 4;
            this.metroButtonSoucres.Text = "选择";
            this.metroButtonSoucres.Click += new System.EventHandler(this.metroButtonSoucres_Click);
            // 
            // dmTextBoxTargetPath
            // 
            this.dmTextBoxTargetPath.AllowDrop = true;
            this.dmTextBoxTargetPath.BackColor = System.Drawing.Color.Snow;
            this.dmTextBoxTargetPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dmTextBoxTargetPath.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.dmTextBoxTargetPath.Location = new System.Drawing.Point(90, 124);
            this.dmTextBoxTargetPath.Name = "dmTextBoxTargetPath";
            this.dmTextBoxTargetPath.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dmTextBoxTargetPath.Size = new System.Drawing.Size(484, 23);
            this.dmTextBoxTargetPath.TabIndex = 3;
            this.dmTextBoxTargetPath.WaterText = "";
            this.dmTextBoxTargetPath.DragDrop += new System.Windows.Forms.DragEventHandler(this.dmTextBoxTargetPath_DragDrop);
            this.dmTextBoxTargetPath.DragEnter += new System.Windows.Forms.DragEventHandler(this.dmTextBoxTargetPath_DragEnter);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(5, 127);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(79, 20);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "输出目录：";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(6, 87);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(65, 20);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "源文件：";
            // 
            // dmTextBoxSourceFile
            // 
            this.dmTextBoxSourceFile.AllowDrop = true;
            this.dmTextBoxSourceFile.BackColor = System.Drawing.Color.Snow;
            this.dmTextBoxSourceFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dmTextBoxSourceFile.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.dmTextBoxSourceFile.Location = new System.Drawing.Point(90, 84);
            this.dmTextBoxSourceFile.Name = "dmTextBoxSourceFile";
            this.dmTextBoxSourceFile.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dmTextBoxSourceFile.Size = new System.Drawing.Size(484, 23);
            this.dmTextBoxSourceFile.TabIndex = 0;
            this.dmTextBoxSourceFile.WaterText = "";
            this.dmTextBoxSourceFile.DragDrop += new System.Windows.Forms.DragEventHandler(this.dmTextBoxSourceFile_DragDrop);
            this.dmTextBoxSourceFile.DragEnter += new System.Windows.Forms.DragEventHandler(this.dmTextBoxSourceFile_DragEnter);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.metroLabel1);
            this.tabPage2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage2.Location = new System.Drawing.Point(4, 36);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(684, 424);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "文件名修改";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(16, 13);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(60, 20);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "建设中...";
            // 
            // dmIcon1
            // 
            this.dmIcon1.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon1.DM_Color = System.Drawing.Color.Black;
            this.dmIcon1.DM_Font_Size = 18F;
            this.dmIcon1.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.记录2;
            this.dmIcon1.DM_Text = "  X-File";
            this.dmIcon1.Location = new System.Drawing.Point(5, 13);
            this.dmIcon1.Name = "dmIcon1";
            this.dmIcon1.Size = new System.Drawing.Size(156, 31);
            this.dmIcon1.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(700, 520);
            this.Controls.Add(this.dmIcon1);
            this.Controls.Add(this.dmTabControl);
            this.DM_howBorder = false;
            this.DM_Mobile = DMSkin.MobileStyle.TitleMobile;
            this.DM_Radius = 4;
            this.DM_ShadowColor = System.Drawing.Color.Bisque;
            this.DM_ShadowWidth = 8;
            this.DM_SystemButtonThemeColor = DMSkin.DMSkinForm.DMColor.Dark;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "X-File";
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.dmTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelSplitSize.ResumeLayout(false);
            this.panelSplitSize.PerformLayout();
            this.panelSplitType.ResumeLayout(false);
            this.panelSplitType.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DMSkin.Controls.DMTabControl dmTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private DMSkin.Controls.DM.DMIcon dmIcon1;
        private DMSkin.Metro.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private DMSkin.Controls.DMTextBox dmTextBoxSourceFile;
        private DMSkin.Metro.Controls.MetroLabel metroLabel2;
        private DMSkin.Controls.DMTextBox dmTextBoxTargetPath;
        private DMSkin.Metro.Controls.MetroLabel metroLabel3;
        private DMSkin.Metro.Controls.MetroButton metroButtonTargetPath;
        private DMSkin.Metro.Controls.MetroButton metroButtonSoucres;
        private System.Windows.Forms.ProgressBar progressBar;
        private DMSkin.Metro.Controls.MetroLabel metroLabel5;
        private DMSkin.Metro.Controls.MetroLabel metroLabel4;
        private DMSkin.Controls.DMTextBox dmTextBoxLog;
        private DMSkin.Metro.Controls.MetroRadioButton metroRadioButton3;
        private DMSkin.Metro.Controls.MetroRadioButton metroRadioButton2;
        private DMSkin.Metro.Controls.MetroRadioButton metroRadioButton1;
        private DMSkin.Controls.DMTextBox dmTextBoxOther;
        private DMSkin.Metro.Controls.MetroLabel metroLabel6;
        private System.Windows.Forms.Panel panelSplitType;
        private System.Windows.Forms.Panel panelSplitSize;
        private DMSkin.Metro.Controls.MetroRadioButton metroRadioButtonSizeTwo;
        private DMSkin.Metro.Controls.MetroRadioButton metroRadioButtonSizeOther;
        private DMSkin.Metro.Controls.MetroRadioButton metroRadioButtonSizeOne;
        private System.Windows.Forms.GroupBox groupBox3;
        private DMSkin.Metro.Controls.MetroButton metroButtonStart;
    }
}

